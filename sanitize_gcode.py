#!/usr/bin/env python3
import re
import sys
from collections import namedtuple
from decimal import Decimal
from itertools import chain
from pathlib import Path
from typing import Optional
from typing import Tuple


PEN_OFFSET = [Decimal(x) for x in ("-54", "20", "0.9")]
ADJUST_OFFSET = [Decimal(x) for x in ("84", "62", "0")]
MINS = [Decimal(x) for x in ("0", "0", "0.9")]
MAXES = [Decimal(x) for x in ("160", "200", "50")]


Point = namedtuple("Point", ["x", "y"])
Size = namedtuple("Size", ["x", "y"])


def apply_offset(param: str, origin: Point) -> str:
    """
    Apply an offset from PEN_OFFSET to a G1 parameter, as necessary.
    """
    if param[0] not in "XYZ":
        return param

    axis, value = param[0], Decimal(param[1:])

    if axis == "X":
        value -= origin.x
    if axis == "Y":
        value -= origin.y

    axis_index = "XYZ".index(axis)
    value += Decimal(PEN_OFFSET[axis_index])
    value += Decimal(ADJUST_OFFSET[axis_index])
    value = max(MINS[axis_index], min(MAXES[axis_index], value))
    return axis + str(value)


def process_line(line: str, origin: Point, size: Size) -> Optional[str]:
    command, comment = re.search(  # type: ignore
        r"^(?P<command>.*?)(?P<comment>\W*(?:\(.*\)|;.*))?$", line
    ).groups()

    if comment:
        stripped_comment = comment.strip("() ;")
        line = f"{command} ; {stripped_comment}\n".strip(" ")

    if re.search(r"^(M104|M109|M140|M190|M105|M106|M107)( .*)?$", command):
        # Blacklisted lines.
        return None

    if not command:
        # Preserve comments and blank lines as they are.
        return line

    if re.match(r"(G(28|91|90)|M(84|106|82))($|\W)", command):
        # Whitelist some commands.
        return line

    if re.match("G0?[01] ", command):
        # Remove extruder moves.
        coords = command.split(" ")[1:]
        # Get the first character of each parameters.
        axes = "".join(sorted([x[0] for x in coords]))
        if axes in ["E", "EF", "S"]:
            # If it's just an extruder move or some weird spindle crap, skip it.
            return None
        else:
            new_coords = []
            for coord in coords:
                if coord[0] in "ES":
                    continue
                new_coords.append(apply_offset(coord, origin))
            if not new_coords:
                return None
            return "G1 " + " ".join(new_coords) + "\n"

    # Skip all lines we don't understand.
    return None


def filtered_gcode_lines(input_stream):
    """
    Remove gcode that we do not wish to process or have appear in the output.
    """
    # Read through the preamble to get it out of the way.
    for line in input_stream:
        if "preamble done" in line.lower():
            break
    else:
        sys.exit("Preamble comment not found.")

    for line in input_stream:
        if "postamble" in line.lower():
            # Ignore everything after the postamble marker comment.
            break

        # Ignore all commands not in the first layer.
        m = re.match(r";\s*LAYER:(\d+)$", line.strip("\n"))
        if m:
            if int(m.group(1)):
                break

        yield line


def find_extents(input_stream) -> Tuple[Point, Size]:
    """
    Find the extents of the x/y values for all G0/G1 commands.
    """
    min_x = min_y = Decimal("Inf")
    max_x = max_y = Decimal("-Inf")
    for line in filtered_gcode_lines(input_stream):
        search_results = re.search(
            r"^G0?[01]"
            r"[^XY]*(?:X(?P<x_value>\d*(?:\.\d+)?))?"
            r"[^Y]*(?:Y(?P<y_value>\d*(?:\.\d+)?))?.*$",
            line,
        )
        if not search_results:
            continue

        x_value, y_value = search_results.groups()

        if x_value:
            decimal_x = Decimal(x_value)
            min_x = min(min_x, decimal_x)
            max_x = max(max_x, decimal_x)
        if y_value:
            decimal_y = Decimal(y_value)
            min_y = min(min_y, decimal_y)
            max_y = max(max_y, decimal_y)

    return Point(min_x, min_y), Size(max_x - min_x, max_y - min_y)


def with_replaced_pre_post_amble(input_stream):
    """
    Add back in the preamble and postamble from the "gcode" directory.

    These would otherwise get filtered out by filtered_gcode_lines().
    """
    this_script_directory = Path(__file__).resolve().parents[0]
    yield from chain(
        open(this_script_directory / "gcode" / "preamble.gcode"),
        filtered_gcode_lines(input_stream),
        open(this_script_directory / "gcode" / "postamble.gcode"),
    )


def main(infile, outdir=None):
    p = Path(infile)
    i = p.open()

    # Figure out the min/max of the draw coordinates so that we can later
    # set the origin to the top left.
    origin, size = find_extents(i)

    i.seek(0)  # Rewind the file pointer to the beginning.

    outfile = p.with_name("sanitized_" + p.stem + ".gcode")
    if outdir:
        outfile = Path(outdir) / outfile.name

    print(f"Saving to {outfile}...")
    o = outfile.open("w")
    for line in with_replaced_pre_post_amble(i):
        output = process_line(line, origin, size)
        if output is not None:
            o.write(output)
    i.close()
    o.close()


if __name__ == "__main__":
    print("Sanitizing...")
    if len(sys.argv) == 2:
        main(sys.argv[1])
    elif len(sys.argv) == 3:
        main(sys.argv[1], sys.argv[2])
    else:
        sys.exit("Input file not specified.")
    print("Done.")

PCB Plotting
============

This repository contains information about a way to easily fabricate PCBs at home that I've been working on.
It consists of various parts.
The general workflow is:

* Design your PCB wherever (KiCAD recommended).
* Export the Gerbers.
* Run pcb2gcode to convert the Gerbers to Gcode for a CNC mill.
* Run `sanitize_gcode.py` to make the Gcode suitable for running on a Marlin 3D printer.
* Mount a permanent marker onto your printer and run the file you generated in the previous step.
  This will draw the layer onto the PCB.
* Throw the PCB into a tub of etchant.
* As a bonus, if your 3D printer has a movable bed, put the etchant onto it and run `agitator.gcode`.
  You're welcome.
* Done.


## Step 1

The first step is to export the thing you want to plot.
It will usually be a circuit board/Gerber files, but it might be images (as in the case of my dickbutt PCB).

### Exporting an image

If you have a raster image you want to draw, just put it in Cura and lower it until the bottom layer is the thing you want to actually plot.
You may want to adjust the first layer speed, but temperature/extrude/etc commands don't matter, as we will strip them out.

You also need to add a comment to your machine start and end Gcode.
Add "; Preamble done." at the end of your start Gcode and "; Postamble." at the start of your end Gcode.

Then, just export the sliced file into a Gcode file and you're done.

### Exporting Gerber files

Install pcb2gcode (at least version 2.0.0), and run it:

```
pcb2gcode --front yourcopperlayer.gbr
```

Done.

This uses the provided `millproject` file, you might need to adjust `mill-diameters` if your marker tip has a different diameter from mine.


## Step 2

You now need to run `./sanitize_gcode.py` on the exported Gcode file to sanitize it for plotting.
This will replace the preamble/postamble, remove any layer above the first, remove temperature and extrude commands, etc.

Run it with the above Gcode file as the first argument:

```
./sanitize_gcode.py output/front.ngc
```

This should finish successfully as long as you added the preamble comments from the previous step (or used `pcb2gcode`).


## Step 3

You now need to mount a permanent marker onto your printer.
I just designed and printed a small cylindrical holder and added rubber bands, but you may want to do something better for stability.
If you do, please let me know, I'm always looking for something better as well.

The marker obviously needs to be mounted lower than your nozzle, so the nozzle doesn't touch the bed/board while plotting.
I have an autoleveling probe which makes this easier, though a bit fiddly.
Adjust your Z endstop accordingly.


## Step 4

You now start the print with the sanitized Gcode file, it will hopefully plot the circuit onto the copper.
Check for bridging and fill out any parts that haven't been filled in.
If you need to redo it, just wipe the board with some alcohol and start again.


## Step 5

You're ready to etch the board!
Carefully put whatever etchant you use into a container and throw the PCB in.

If your printer has a movable bed/Y axis, I have included `output/agitator.gcode`, which you can use to agitate the etchant to get a faster etch.
Just run the script on an empty bed, wait for it to home, and put the container with the etchant onto the bed when the print head moves out of the way.
It will rock for about thirty minutes.

Is there anything 3D printers can't do?!


## Step 6

You're pretty much done!
Carefully remove the PCB from the bath, wipe the marker away with alcohol, and enjoy your new PCB!


# Acknowledgements

My many thanks go to my friend [Andrew Top](https://andrewtop.com/), without whose valuable contribution the dickbutt PCB may never have been.

-- Stavros
